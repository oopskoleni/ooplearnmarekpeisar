package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Square extends Shape {

    private Integer side;

    private String name;
    private String color;
    private long Id;

    public Square(Integer side, String color, String name, long id) {
        this.side = side;
        this.color = color;
        this.name = name;
        this.Id = id;
    }

    public Integer getArea() {
        return 4 * side;
    }

    public Integer getPerimeter() {
        return side * side;
    }

    public void printDetailOfItSelf() {
        System.out.println("type: Square\ncolor: " + color + "\nname: " + name + "\nId: " + Id + "\nArea: " + getArea() + "\nPerimeter: " + getPerimeter());
    }
}
