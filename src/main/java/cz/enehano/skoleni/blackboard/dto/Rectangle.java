package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Rectangle extends Shape {

    private Integer sideA;
    private Integer sideB;

    private String name;
    private String color;
    private long Id;

    public Rectangle(Integer sideA, Integer sideB, String color, String name, long id) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.color = color;
        this.name = name;
        this.Id = id;
    }

    public Integer getArea() {
        return 2 * (sideA + sideB);
    }

    public Integer getPerimeter() {
        return sideA * sideB;
    }

    public void printDetailOfItSelf() {
        System.out.println("type: Rectangle\ncolor: " + color + "\nname: " + name + "\nId: " + Id + "\nArea: " + getArea() + "\nPerimeter: " + getPerimeter());
    }


}
