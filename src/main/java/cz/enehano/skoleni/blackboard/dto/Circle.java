package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Circle extends Shape {

    private Integer radius;

    private String name;
    private String color;
    private long Id;

    public Circle(Integer radius, String color, String name, long id) {
        this.radius = radius;
        this.color = color;
        this.name = name;
        this.Id = id;
    }

    public Integer getArea() {
        return Math.round(Math.round(2 * Math.PI * radius));
    }

    public Integer getPerimeter() {
        return Math.round(Math.round(Math.PI * radius * radius));
    }

    public void printDetailOfItSelf() {
        System.out.println("type: Circle\ncolor: " + color + "\nname: " + name + "\nId: " + Id + "\nArea: " + getArea() + "\nPerimeter: " + getPerimeter());
    }

}
