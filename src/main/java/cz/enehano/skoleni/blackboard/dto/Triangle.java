package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Triangle extends Shape {

    private Integer sideA;
    private Integer sideB;
    private Integer sideC;
    private Integer heightC;

    private String name;
    private String color;
    private long Id;

    public Triangle(Integer sideA, Integer sideB, Integer sideC, Integer heightC, String color, String name, long id) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        this.heightC = heightC;
        this.name = name;
        this.color = color;
        this.Id = id;
    }

    public Integer getArea() {
        return sideA + sideB + sideC;
    }

    public Integer getPerimeter() {
        return (heightC * sideC) / 2;
    }

    public void printDetailOfItSelf() {
        System.out.println("type: Triangle\ncolor: " + color + "\nname: " + name + "\nId: " + Id + "\nArea: " + getArea() + "\nPerimeter: " + getPerimeter());
    }
}
