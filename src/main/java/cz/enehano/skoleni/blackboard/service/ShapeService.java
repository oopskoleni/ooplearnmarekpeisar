package cz.enehano.skoleni.blackboard.service;

import cz.enehano.skoleni.blackboard.dto.Circle;
import cz.enehano.skoleni.blackboard.dto.Rectangle;
import cz.enehano.skoleni.blackboard.dto.Square;
import cz.enehano.skoleni.blackboard.dto.Triangle;

public class ShapeService {

    /**
     * In this class will be all operation with shape
     *  - create new share
     *  - edit shape
     *  - etc.
     */

    public void createSquare(Integer side, String color, String name, long Id) {
        new Square(side, color, name, Id);
    }

    public void createRectange(Integer sideA, Integer sideB, String color, String name, long Id) {
        new Rectangle(sideA, sideB, color, name, Id);
    }

    public void createTriangle(Integer sideA, Integer sideB, Integer sideC, Integer heightC, String color, String name, long Id) {
        new Triangle(sideA, sideB, sideC, heightC, color, name, Id);
    }

    public void createCircle(Integer radius, String color, String name, long Id) {
        new Circle(radius, color, name, Id);
    }

    public void editShape() {

    }
}
