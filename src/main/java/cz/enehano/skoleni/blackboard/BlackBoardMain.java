package cz.enehano.skoleni.blackboard;

import cz.enehano.skoleni.blackboard.dto.Shape;
import cz.enehano.skoleni.blackboard.service.BlackBoardService;
import cz.enehano.skoleni.blackboard.service.ShapeService;

public class BlackBoardMain {

    public static void main(String[] args) {
        BlackBoardService boardService = new BlackBoardService();
        boardService.printAllShapes();
    }
}
